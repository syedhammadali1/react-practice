import './App.css';
import Navbar from './Components/Navbar';
import TextForm from './Components/TextForm';
import React, { useEffect, useState } from 'react';
import { Link, Route, Routes, Switch } from 'react-router-dom';
import ShowFormData from './Components/ShowFormData';
import TitleCount from './Components/TitleCount';
import ShowScreenSize from './Components/ShowScreenSize';
import PlayingWithSpreadOperator from './Components/PlayingWithSpreadOperator';
import PlayingWithUseReducerHook from './Components/PlayingWithUseReducerHook';
import ComA from './Components/PlayingWithUseContext/ComA';
import { AnimatidLogo, SecondButton, StyledButton, SubmitButton, SubmitButton2, ThemeButton } from './Components/button/button.styles';
import logo from './logo.svg'
import { createGlobalStyle, ThemeProvider } from 'styled-components';
// import { Switch } from 'react-router-dom';


const theme = {
  dark: {
    primary: 'darkblue',
    text: 'white'
  },
  light: {
    primary: 'white',
    text: 'black'
  },
  fontFamily: 'serf',
}
const GlobalStyle = createGlobalStyle`
button{
  /* height: 100px; */
  font-family: ${(props)=> props.theme.fontFamily};
}
`
const App = () => {

  const [val, setVal] = useState(['by', 'shy', 'lay'])
  const [testLoading, setTestLoading] = useState(true)

  useEffect(() => {
    setTimeout(() => {
      setTestLoading(false)
    }, 3000);

  }, [])


  const toggle = () => {
    // if(val == 'by'){
    //   setVal('hy')
    // }else{
    //   setVal('by')
    // }
  }

  // var a = "";
  // const  [users,setUsers] = useState([]);
  // const getUser = async () => {
  //   var response = await fetch('https://jsonplaceholder.typicode.com/posts')
  //   console.log(response.json());
  //   //  setUsers()

  //   } 

  var myBio = [
    {
      id: 1, name: "hammad", age: 22
    },
    {
      id: 2, name: "arsalan", age: 21
    }
  ]

  const clearData = () => {
    // console.log('clear');
    setArray([]);
  }

  const [array, setArray] = useState(myBio)

  return (
    <>
      {/* { 
  users.data.map((ele) => {
 return (
   <div> 
     <p> {ele.id}</p>

   </div>
 )
  })
} */}
      <ThemeProvider theme={theme}>
      <GlobalStyle />
        <Link name="" id="" class="btn btn-primary" to="/text" role="button">text</Link>
        <Link name="" id="" class="btn btn-primary" to="/" role="button">home</Link>
        <Link name="" id="" class="btn btn-primary" to="/ShowFormData" role="button">ShowFormData</Link>
        <button onClick={toggle}> {val[0]} </button>
        <Routes>
          <Route path="/" element={<Navbar title="hammad" aboutus="afterDefault" />} />
          <Route path="/ShowFormData" element={<ShowFormData />} />
        </Routes>

        {/* {
   array.map((element)=>{
   
    return (<>
    <h1>{element.name}</h1> <h2>{element.age}</h2> <br></br>
   </>
    )
   })
 } */}

        <button onClick={clearData}>cleardata</button>

        <Routes>
          <Route path="/text" element={<TextForm />} />
        </Routes>
        <br />

        <div>
          <TitleCount />
        </div>



        <br />

        <div>
          <ShowScreenSize />
        </div>

        <br />

        <div>
          <PlayingWithSpreadOperator />
        </div>

        <br />

        <div>
          <PlayingWithUseReducerHook />
        </div>

        <br />

        <div>
          <ComA />
        </div>


        {testLoading && <div>
          loading....
        </div>}

        <div>
          {/* <button>styled component</button> */}
          <StyledButton theme='black' type="button">styled Button with props</StyledButton>
          <br />
          <br />
          <StyledButton type="button" className='btn btn-primary '>styled Button</StyledButton>
          <br />
          <br />
          <SecondButton as='a'>second</SecondButton>
          <br />
          <br />
          <SubmitButton>submit buttton</SubmitButton>
          <br />
          <br />
          <SubmitButton2 userName='hammad'>submit buttton 2</SubmitButton2>

          <br />
          <br />
          <AnimatidLogo src={logo}></AnimatidLogo>
          <br />
          <br />
          <ThemeButton>google</ThemeButton>
        </div>

      </ThemeProvider>

    </>
  );
}

export default App;




