import React, { memo, useContext } from 'react'
import {UserName} from './ComA'

const ComC = () => {
    const Name = useContext(UserName)
    console.log('Comc');
  return (
    <>{Name}</>
  )
}

export default memo(ComC)