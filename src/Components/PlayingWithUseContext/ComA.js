import React, { createContext, useState } from 'react'
import ComB from './ComB'
const UserName = createContext();

const ComA = () => {
    const [user, setUser] = useState('hammad')
  return (
    <>
    <UserName.Provider value={user}>
    <ComB/>
    </UserName.Provider>
    </>
  )
}

export default ComA;
export { UserName }; 