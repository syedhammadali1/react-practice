import React, { useState } from 'react'
import ComC from './ComC'

const ComB = () => {
  const [first, setfirst] = useState(0)
  const [second, setSecond] = useState(1)
  return (
    <>
    {first}
    <ComC second={second}/>
    <button onClick={()=> setfirst(first+1) }> first memo use</button>
    <button onClick={()=> setSecond(second+1) }>second memo use</button>
    </>
  )
}

export default ComB