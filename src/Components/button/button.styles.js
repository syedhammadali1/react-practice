import styled, { keyframes } from 'styled-components'

export const StyledButton = styled.button`
background-color: ${(props) => props.theme === 'black' ? 'lightgreen' : 'black'};
color:${(props) => props.theme === 'black' ? 'black' : 'white'};
&:hover{
    background-color: ${(props) => props.theme !== 'black' ? 'lightgreen' : 'black'};
     color:${(props) => props.theme !== 'black' ? 'black' : 'white'};

}
`
export const SecondButton = styled(StyledButton)`
background-image: linear-gradient( to right, pink 20%,green 100%);
`
export const SubmitButton = styled(StyledButton).attrs({
    type : 'submit'
})`
background-image: linear-gradient( to right, pink 20%,green 100%);
`
export const SubmitButton2 = styled(StyledButton).attrs((props) =>({
    type : 'submit',
    id:props.userName
}))`
background-image: linear-gradient( to right, pink 20%,green 100%);
`

const rotate = keyframes`
from{
    transform: rotate(0deg);
}
to{
    transform: rotate(360deg);
}
`
export const AnimatidLogo = styled.img`
height: 40vmin;
pointer-events: none;
animation: ${rotate} infinite 20s linear;
`


export const ThemeButton = styled(StyledButton)`
background-color: ${(props) => props.theme.dark.primary};
color: ${(props) => props.theme.dark.text};
`