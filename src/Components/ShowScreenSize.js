import React, { useEffect, useState } from 'react'

function ShowScreenSize() {
    const [screenSize, setScreenSize] = useState(window.innerWidth);
    const changeSize = () => {
        setScreenSize(window.innerWidth);    
    }
    useEffect(() => {
        // setScreenSize(screenSize)
        window.addEventListener('resize',changeSize);
      return () => {
        window.removeEventListener('resize',changeSize);
      }
    })
    
 

  return (
    <>
    <h1>screen size</h1>
    <h1>{screenSize}</h1>

    </>
  )
}

export default ShowScreenSize