import React from 'react'
import { useReducer } from 'react';
import { useHistory, useNavigate } from "react-router-dom";

const initialState = { count: 0 };

const reducer = (state, action) => {
  if (action.type === 'increment') {
    return { count: state.count + 1 };
  }
};

const PlayingWithUseReducerHook = () => {
  const navigate = useNavigate();


  // console.log(navigate);
  const [state, dispatch] = useReducer(reducer, initialState);

  return (
    <>
      Count: {state.count}
      <button onClick={() => dispatch({ type: 'increment' })}>
        Click me
      </button>
    </>

  )
}

export default PlayingWithUseReducerHook


