import React, { useState } from 'react'

function PlayingWithSpreadOperator() {
    // const detail = {name:'hammad',age:22};
    // console.log('PlayingWithSpreadOperator');

    const [detail, setDetail] = useState({name:'hammad',age:22});
    const updateName = () => {
        setDetail({...detail,name:'shah'});
    }

    return (
        <>
            name:{detail.name}<br></br>
            age:{detail.age}<br></br>
        <button onClick={updateName}>update</button>
        </>
    )
}

export default PlayingWithSpreadOperator