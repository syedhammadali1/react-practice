import React, {useState} from 'react'

export default function TextForm() {

  // const [value, setValue] = useState(1);
    const [text,setText] = useState("hello sir!");
    const changeText = (event)=>{
     setText(event.target.value);
    }
    const makeUpperCase = (event)=>{
      let newText = text.toUpperCase();
     setText(newText);
    }
  return (
    <>
  
<div className="mb-3 container">
  <textarea className="form-control" id="exampleFormControlTextarea1" rows="3" value={text} onChange={changeText}></textarea>
  <button className="btn btn-primary" onClick={makeUpperCase}>click to convert uppercase</button>
</div>
    </>
  )
}



