import React, { useEffect, useState } from 'react'
import{ useCount} from './createCustomHook/useCount';

function TitleCount() {
    const [titleCount, settitleCount] = useState(0);
    const increaseTitleCount = () => {
        settitleCount(titleCount + 1);
    }
    // useEffect(() => {
    //     if(titleCount > 0){
    //         document.title = `count(${titleCount}) `
    //     }else{
    //         document.title = `count`
    //     }
    // },[])

    useCount(titleCount);
    

  return (
    <>
    {titleCount} ++
    <button onClick={increaseTitleCount}>increaseTitleCount</button>
    </>
  )
}

export default TitleCount