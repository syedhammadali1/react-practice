import React, { useEffect } from 'react'

const useCount = (titleCount) => {
    useEffect(() => {
        if(titleCount > 0){
            document.title = `count(${titleCount}) `
        }else{
            document.title = `count`
        }
    },[])
  return (
    <></>
  )
}

export {useCount};